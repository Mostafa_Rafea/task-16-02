<?php include_once 'layouts/header.php'; ?>

<?php
// Facebook url handle
if(!empty($_POST['facebook']) && !preg_match("/facebook\.com+\/[a-zA-Z0-9._]{1,50}$/", $_POST['facebook'])) {
    $error = 'Enter valid facebook URL';
    exit(header("Location: index.php?error=$error"));
}
// Instagram url handle
if(!empty($_POST['instagram']) && !preg_match("/instagram\.com+\/[a-zA-Z0-9._]{1,50}$/", $_POST['instagram'])) {
    $error = 'Enter valid instagram URL';
    exit(header("Location: index.php?error=$error"));
}
?>

<table class="table table-dark">
    <thead>
        <tr>
            <th scope="col">Selector</th>
            <th scope="col">Option</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($_POST as $selector => $option):
            if(!empty($option) && $option != "None") :
        ?>
        <tr>
            <td><?= ucfirst($selector) ?></td>
            <td><?= $option ?></td>
        </tr>
        <?php
        endif;
        endforeach; 
        ?>
    </tbody>


    <?php include_once 'layouts/footer.php'; ?>