<?php

include_once 'layouts/header.php';

$selects = [
    'user' => ["One", "Two", "Three"],
    'avatar' => ["One", "Two", "Three", "Four"],
    'model' => ["Computer", "Mobile", "Server", "Laptop", "Tablet", "TV"],
    'nationality' => ["Egyptian", "Italian", "Spanish", "English", "French", "German", "Brazilian", "Argentinian", "Saudi", "Saurian", "Lebanese", "Portugal", "Austrian", "Finlander"],
    'skin' => ["Neutral", "Cool", "Warm"],
    'hair_type' => ["One", "Two", "Three", "Four"],
    'hair_color' => ["One", "Two", "Three", "Four"],
    'eye_color' => ["One", "Two", "Three", "Four"],
    'gender' => ["Male", "Female"],
    'body_shape' => ["One", "Two", "Three", "Four"],
];

$birthday = [
    'day' => ['start' => 1, 'end' => 31],
    'month' => ['start' => 1, 'end' => 12],
    'year' => ['start' => 1900, 'end' => 2019]
];

$creation = [
    'day' => ['start' => 1, 'end' => 31],
    'month' => ['start' => 1, 'end' => 12],
    'year' => ['start' => 1900, 'end' => 2019],
    'hour' => ['start' => 0, 'end' => 23],
    'minute' => ['start' => 0, 'end' => 59],
]

?>
<h3 class="text-center pb-3">Registration Form</h3>
<form method="post" action="action.php">
    <div class="offset-2 col-5">
        <?php if(isset($_GET['error'])) :?>
        <h5 class="alert alert-danger" role="alert"><?= $_GET['error'] ?></h5>
        <?php endif; ?>
        <div class="form-group">
            <label for="fbook">Facebook</label>
            <input type="url" name="facebook" id="fbook" class="form-control"
                placeholder="example => https://www.facebook.com/zuck" />
        </div>
        <div class="form-group">
            <label for="insta">Instagram</label>
            <input type="url" id="insta" name="instagram" class="form-control"
                placeholder="example => https://www.instagram.com/mostafa.rafea/" />
        </div>
        <label>Birth Date</label>
        <div class="form-group row ml-1">
            <?php foreach($birthday as $key => $item): ?>
            <select name=<?= 'birth_'.$key ?> class="custom-select col-3">
                <option>None</option>
                <?php for($i=$item['start']; $i<=$item['end']; $i++):?>
                <option><?= $i ?></option>
                <?php endfor; ?>
            </select>
            <?php endforeach; ?>
        </div>
        <div class="custom-control custom-checkbox mb-3 ml-3">
            <input type="checkbox" class="custom-control-input" name="empty" id="exampleCheck1">
            <label class="custom-control-label" for="exampleCheck1" style="font-size: 15px">Leave
                empty</label>
        </div>
        <div class="form-group">
            <label for="edu">Education</label>
            <input type="text" id="edu" name="education" class="form-control" placeholder="Software Engineering ..." />
        </div>
        <div class="form-group">
            <label for="_bio">Bio</label>
            <textarea id="_bio" name="bio" class="form-control" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="_price">Price</label>
            <input type="number" id="_price" name="price" class="form-control" value="0" disabled />
        </div>
        <div class="form-group">
            <label for="_points">Points</label>
            <input type="number" id="_points" name="points" class="form-control" value="0" disabled />
        </div>
        <div class="custom-control custom-checkbox mb-3 ml-3">
            <input type="checkbox" name="enabled" class="custom-control-input" id="exampleCheck2">
            <label class="custom-control-label" for="exampleCheck2" style="font-size: 15px">Enabled
                <sup style="color: red; font-size: 20px;">.</sup></label>
        </div>
        <div class="form-group">
            <label for="_tattoos">Tattoos</label>
            <textarea id="_tattoos" name="tattoos" class="form-control" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="_scars">Scars</label>
            <textarea id="_scars" name="scars" class="form-control" rows="5"></textarea>
        </div>
        <div class="form-group">
            <label for="_talents">Talents</label>
            <textarea id="_talents" name="talents" class="form-control" rows="5"></textarea>
        </div>
        <label>Created at</label>
        <div class="form-group row ml-1">
            <?php foreach($creation as $key => $item): ?>
            <select name=<?= 'creation_'.$key ?> class="custom-select col-2">
                <option>None</option>
                <?php for($i=$item['start']; $i<=$item['end']; $i++):?>
                <option><?= $i ?></option>
                <?php endfor; ?>
            </select>
            <?php endforeach; ?>
        </div>
        <div class="custom-control custom-checkbox mb-3 ml-3">
            <input type="checkbox" name="empty2" class="custom-control-input" id="exampleCheck3">
            <label class="custom-control-label" for="exampleCheck3" style="font-size: 15px">Leave
                empty</label>
        </div>

        <?php foreach($selects as $key => $select) : ?>
        <div class="form-group">
            <label for=<?= '_'.$key ?>><?= ucfirst($key) ?></label>
            <select class="custom-select" name=<?= $key ?> id=<?= '_'.$key ?>>
                <option>None</option>
                <?php foreach($select as $option) : ?>
                <option><?= $option ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <?php endforeach; ?>


        <div class="form-group">
            <label for="_languages">Languages <span style="color: red;">*</span></label>
            <input type="text" id="_languages" name="languages" class="form-control"
                placeholder="English, German ..." />
        </div>
        <div class="form-group">
            <label for="_auditions">Auditions</label>
            <input type="text" id="_auditions" name="auditions" class="form-control" />
        </div>
        <div class="form-group">
            <label for="_calenders">Calenders</label>
            <input type="text" id="_calenders" name="Calenders" class="form-control" />
        </div>
        <div class="form-group">
            <label for="_work">Work experience</label>
            <input type="text" id="_work" name="work" class="form-control" />
        </div>
        <div class="form-group">
            <input type="submit" class="btnSubmit" />
        </div>
    </div>
</form>

<?php include_once 'layouts/footer.php'; ?>