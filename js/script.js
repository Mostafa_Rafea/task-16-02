// Leave empty checkbox for birth date
$('input[name="empty"]').on('click', function () {
    var thisCheck = $(this);
    if (thisCheck.is(':checked')) {
        $('input[name="birth_date"]').attr('disabled', 'disabled');
    } else {
        $('input[name="birth_date"]').removeAttr('disabled');
    }
})

// Enabled checkbox for price & points
$('input[name="enabled"]').on('click', function () {
    var Check = $(this);
    if (Check.is(':checked')) {
        $('input[name="price"]').removeAttr('disabled');
        $('input[name="points"]').removeAttr('disabled');
    } else {
        $('input[name="price"]').attr('disabled', 'disabled');
        $('input[name="points"]').attr('disabled', 'disabled');
    }
})

// Leave empty checkbox for creation date time
$('input[name="empty2"]').on('click', function () {
    var checkThis = $(this);
    if (checkThis.is(':checked')) {
        $('input[name="creation"]').attr('disabled', 'disabled');
    } else {
        $('input[name="creation"]').removeAttr('disabled');
    }
})